/* 
 * Solves the Aliev-Panfilov model  using an explicit numerical scheme.
 * Based on code orginally provided by Xing Cai, Simula Research Laboratory
 * 
 * Modified and  restructured by Scott B. Baden, UCSD
 * 
 */
#ifdef SSE_VEC
#define AVX 1
//#define SSE 1
#endif

#include <assert.h>
#include <stdlib.h>
#include <malloc.h>
#include <iostream>
#include <iomanip>
#include <string>
#include <math.h>
#include "time.h"
#include "apf.h"
#include "Plotting.h"
#include "cblock.h"
#include <emmintrin.h>
#ifdef AVX
#include <immintrin.h>
#endif
#ifdef _MPI_
#include <mpi.h>
#endif
using namespace std;

void repNorms(double l2norm, double mx, double dt, int m,int n, int niter, int stats_freq);
void stats(double *E, int m, int n, double *_mx, double *sumSq);

extern control_block cb;
extern int m, n;
extern int dx, dy;
extern double *send_left_buf, *send_right_buf, *recv_left_buf, *recv_right_buf;
extern double *Plot_E;
// extern int m, n;

#ifdef SSE_VEC
// If you intend to vectorize using SSE instructions, you must
// disable the compiler's auto-vectorizer
__attribute__((optimize("no-tree-vectorize")))
#endif 

// The L2 norm of an array is computed by taking sum of the squares
// of each element, normalizing by dividing by the number of points
// and then taking the sequare root of the result
//
double L2Norm(double sumSq){
	double l2norm = sumSq /  (double) ((cb.m)*(cb.n));
	l2norm = sqrt(l2norm);
	return l2norm;
}

void solve(double **_E, double **_E_prev, double *R, double alpha, double dt, Plotter *plotter, double &L2, double &Linf){

	// Simulated time is different from the integer timestep number
	double t = 0.0;
	double *E = *_E, *E_prev = *_E_prev;
	double *R_tmp = R;
	double *E_tmp = *_E;
	double *E_prev_tmp = *_E_prev;
	double mx, sumSq, root_sumSq, root_Linf;

	int niter;
	int innerBlockRowStartIndex = (n+2)+1;
	int innerBlockRowEndIndex = (((m+2)*(n+2) - 1) - (n)) - (n+2);
#ifdef SSE	
	// Definition of all the SSE varibles used in the code
	__m128d alpha_sse = _mm_set1_pd(alpha);
	register __m128d temp_1, temp_2, temp_3;
	__m128d four_sse = _mm_set1_pd(4);
	__m128d E_tmp_center, R_tmp_center, E_prev_center, E_prev_left, E_prev_right, E_prev_above, E_prev_below;

	// Variables that are needed for the ODE part of the function
	__m128d dt_sse = _mm_set1_pd(dt);
	__m128d minus_dt_sse = _mm_set1_pd(-dt);
	__m128d a_sse = _mm_set1_pd(a);
	__m128d epsilon_sse = _mm_set1_pd(epsilon);
	__m128d M1_sse = _mm_set1_pd(M1);
	__m128d M2_sse = _mm_set1_pd(M2);
	__m128d kk_sse = _mm_set1_pd(kk);
	__m128d minus_b_minus_one_sse = _mm_set1_pd(-b-1);
	__m128d one_sse = _mm_set1_pd(1);
	__m128d zero_sse = _mm_set1_pd(0);
#endif

#ifdef AVX
	// Definition of all the SSE varibles used in the code
	register __m256d temp_1, temp_2, temp_3;
	__m256d alpha_sse = _mm256_set1_pd(alpha);
	__m256d four_sse = _mm256_set1_pd(4);
	__m256d E_tmp_center, R_tmp_center, E_prev_center, E_prev_left, E_prev_right, E_prev_above, E_prev_below;

	// Variables that are needed for the ODE part of the function
	__m256d dt_sse = _mm256_set1_pd(dt);
	__m256d minus_dt_sse = _mm256_set1_pd(-dt);
	__m256d a_sse = _mm256_set1_pd(a);
	__m256d epsilon_sse = _mm256_set1_pd(epsilon);
	__m256d M1_sse = _mm256_set1_pd(M1);
	__m256d M2_sse = _mm256_set1_pd(M2);
	__m256d kk_sse = _mm256_set1_pd(kk);
	__m256d minus_b_minus_one_sse = _mm256_set1_pd(-b-1);
	__m256d one_sse = _mm256_set1_pd(1);
	__m256d zero_sse = _mm256_set1_pd(0);
#endif
	// We continue to sweep over the mesh until the simulation has reached
	// the desired number of iterations
	for (niter = 0; niter < cb.niters; niter++){

		if  (cb.debug && (niter==0)){
			stats(E_prev,m,n,&mx,&sumSq);
			double l2norm = L2Norm(sumSq);
			repNorms(l2norm,mx,dt,m,n,-1, cb.stats_freq);
			if (cb.plot_freq)
				plotter->updatePlot(E,  -1, m+1, n+1);
		}

		/* 
		 * Copy data from boundary of the computational box to the
		 * padding region, set up for differencing computational box's boundary
		 *
		 * These are physical boundary conditions, and are not to be confused
		 * with ghost cells that we would use in an MPI implementation
		 *
		 * The reason why we copy boundary conditions is to avoid
		 * computing single sided differences at the boundaries
		 * which increase the running time of solve()
		 *
		 */

		// 4 FOR LOOPS set up the padding needed for the boundary conditions
		int i,j;

		// Fills in the TOP Ghost Cells
		if(dy == 0)
		{
			for (i = 0; i < (n+2); i++) 
			{
				E_prev[i] = E_prev[i + (n+2)*2];
			}
		}

		// Fills in the RIGHT Ghost Cells
		if(dx == cb.px - 1)
		{
			for (i = (n+1); i < (m+2)*(n+2); i+=(n+2)) 
			{
				E_prev[i] = E_prev[i-2];
			}  
		}


		// Fills in the LEFT Ghost Cells
		if(dx == 0)
		{
			for (i = 0; i < (m+2)*(n+2); i+=(n+2)) 
			{
				E_prev[i] = E_prev[i+2];
			}  
		}


		// Fills in the BOTTOM Ghost Cells
		if(dy == cb.py - 1)
		{
			for (i = ((m+2)*(n+2)-(n+2)); i < (m+2)*(n+2); i++) 
			{
				E_prev[i] = E_prev[i - (n+2)*2];
			}  
		}


#ifdef _MPI_

		// #endif  

		//Pushing the right column of the matrix in the send_right_buf
		if(!cb.noComm)
		{

			if(dx!=0)
			{
				int j = 0;
				//Pushing the left column of the matrix in the send_left_buf
				for(i = n+3; i < (m+1)*(n+2); i += (n+2))
				{
					send_left_buf[j] = E_prev[i];
					j++;
				}
			}

			if(dx!=cb.px-1)
			{
				int j = 0;
				//Pushing the left column of the matrix in the send_left_buf
				for(i = (n+2) + n; i < (m+1)*(n+2); i += (n+2))
				{
					send_right_buf[j] = E_prev[i];
					j++;
				}
			}
			if(cb.px*cb.py> 1)
			{ 
				MPI_Request send[4];
				MPI_Request recv[4];
				MPI_Status stat[4];

				int msgcount = 0;

				// Send data to ghost cells and receive the data to the ghost cells
				if(dx != 0)   
				{
					// Send data to the left block
					MPI_Isend(send_left_buf , m, MPI_DOUBLE, dy*cb.px + dx - 1, 0, MPI_COMM_WORLD, send+msgcount);
					MPI_Irecv(recv_left_buf , m, MPI_DOUBLE, dy*cb.px + dx - 1, 0, MPI_COMM_WORLD, recv+msgcount);
					msgcount++;

				}

				if(dx != cb.px-1)  
				{
					// Send the datat to eh right block
					MPI_Isend(send_right_buf , m, MPI_DOUBLE, dy*cb.px + dx + 1, 0, MPI_COMM_WORLD, send+msgcount);
					MPI_Irecv(recv_right_buf , m, MPI_DOUBLE, dy*cb.px + dx + 1, 0, MPI_COMM_WORLD, recv+msgcount);
					msgcount++;

				}
				if(dy != 0) 
				{
					// Send the data to the up block
					MPI_Isend(E_prev + n + 2 + 1 , n, MPI_DOUBLE, (dy-1)*cb.px + dx, 0, MPI_COMM_WORLD, send+msgcount);
					MPI_Irecv(E_prev + 1         , n, MPI_DOUBLE, (dy-1)*cb.px + dx, 0, MPI_COMM_WORLD, recv+msgcount);
					msgcount++;

				}
				if(dy != cb.py-1)  
				{
					// Send the data to the down block
					MPI_Isend(E_prev +m*(n+2) + 1     , n, MPI_DOUBLE, (dy+1)*cb.px + dx, 0, MPI_COMM_WORLD, send+msgcount);
					MPI_Irecv(E_prev + (m+1)*(n+2) + 1, n, MPI_DOUBLE, (dy+1)*cb.px + dx, 0, MPI_COMM_WORLD, recv+msgcount);
					msgcount++;

				}

				MPI_Waitall(msgcount, recv, stat);

				if(dx !=0)
				{
					int j = 0; 
					for (i = n+2; i < (m+1)*(n+2); i+=(n+2)) 
					{
						E_prev[i] = recv_left_buf[j];
						j++;
					}
				}

				if(dx!=cb.px-1)
				{
					int j = 0;
					for (i = (n+2) + (n+1); i < (m+1)*(n+2); i+=(n+2)) 
					{
						E_prev[i] = recv_right_buf[j];
						j++;
					}
				}  
			}
		}


#endif

		//////////////////////////////////////////////////////////////////////////////
#ifdef SSE_VEC
#ifdef SSE
		// #define FUSED 1
#ifdef FUSED
		// Solve for the excitation, a PDE
		for(j = innerBlockRowStartIndex; j <= innerBlockRowEndIndex; j+=(n+2)) {
			E_tmp = E + j;
			E_prev_tmp = E_prev + j;
			R_tmp = R + j;
			for(int i = 0; i < n; i++) {
				E_tmp[i] = E_prev_tmp[i]+alpha*(E_prev_tmp[i+1]+E_prev_tmp[i-1]-4*E_prev_tmp[i]+E_prev_tmp[i+(n+2)]+E_prev_tmp[i-(n+2)]);
				E_tmp[i] += -dt*(kk*E_tmp[i]*(E_tmp[i]-a)*(E_tmp[i]-1)+E_tmp[i]*R_tmp[i]);
				R_tmp[i] += dt*(epsilon+M1* R_tmp[i]/( E_tmp[i]+M2))*(-R_tmp[i]-kk*E_tmp[i]*(E_tmp[i]-b-1));
			}
		}
#else
		// Solve for the excitation, a PDE
		for(j = innerBlockRowStartIndex; j <= innerBlockRowEndIndex; j+=(n+2)) {
			E_tmp = E + j;
			E_prev_tmp = E_prev + j;
			for(int i = 0; i < n; i+=2) {
				// Storing the values of the left, right, top down E_prev entries in the sse registers 
				E_prev_center = _mm_loadu_pd(E_prev_tmp + i);
				E_prev_left = _mm_loadu_pd(E_prev_tmp + i - 1);
				E_prev_right = _mm_loadu_pd(E_prev_tmp + i + 1);
				E_prev_above = _mm_loadu_pd(E_prev_tmp + i - (n+2));
				E_prev_below = _mm_loadu_pd(E_prev_tmp + i + (n+2));

				// Airthematic operations done to replicated the PDE done in the case without vectorisations
				temp_1 = _mm_mul_pd(four_sse, E_prev_center);		// 4*E_prev_tmp[i]
				temp_2 = _mm_add_pd(E_prev_right, E_prev_left);		// E_prev_tmp[i+1]+E_prev_tmp[i-1]
				temp_3 = _mm_add_pd(E_prev_above, E_prev_below);	// E_prev_tmp[i-(n+2)]+E_prev_tmp[i+(n+2)]
				temp_2 = _mm_add_pd(temp_2, temp_3);				
				temp_1 = _mm_sub_pd(temp_2, temp_1);
				temp_1 = _mm_mul_pd(temp_1, alpha_sse);
				temp_1 = _mm_add_pd(E_prev_center, temp_1);

				// Storing the value back into the right pointer
				_mm_storeu_pd(E_tmp + i, temp_1);

				//E_tmp[i] = E_prev_tmp[i]+alpha*(E_prev_tmp[i+1]+E_prev_tmp[i-1]-4*E_prev_tmp[i]+E_prev_tmp[i+(n+2)]+E_prev_tmp[i-(n+2)]);
			}
		}

		/* 
		 * Solve the ODE, advancing excitation and recovery variables
		 *     to the next timtestep
		 */



		for(j = innerBlockRowStartIndex; j <= innerBlockRowEndIndex; j+=(n+2)) {
			E_tmp = E + j;
			R_tmp = R + j;
			for(int i = 0; i < n; i+=2) {
				// Loading the pointer into 128 bit registers 
				E_tmp_center = _mm_loadu_pd(E_tmp + i);
				R_tmp_center = _mm_loadu_pd(R_tmp + i);	

				// Computation needed to replicate the functionality with vectorisation				
				temp_1 = _mm_mul_pd(E_tmp_center, R_tmp_center);			// E_tmp[i]*R_tmp[i]
				temp_2 = _mm_mul_pd(kk_sse, E_tmp_center);				// kk*E_tmp[i]
				temp_3 = _mm_sub_pd(E_tmp_center, a_sse);				// E_tmp[i] - a
				temp_2 = _mm_mul_pd(temp_2, temp_3);					// kk*E_tmp[i]*(E_tmp[i]-a)
				temp_3 = _mm_sub_pd(E_tmp_center, one_sse);				// E_tmp[i] - 1
				temp_2 = _mm_mul_pd(temp_2, temp_3);					// kk*E_tmp[i]*(E_tmp[i]-a)*(E_tmp[i]-1)
				temp_1 = _mm_add_pd(temp_1, temp_2);					// kk*E_tmp[i]*(E_tmp[i]-a)*(E_tmp[i]-1) + E_tmp*R_tmp 
				temp_1 = _mm_mul_pd(dt_sse, temp_1);					// dt*(kk*E_tmp[i]*(E_tmp[i]-a)*(E_tmp[i]-1)+E_tmp*R_tmp)

				E_tmp_center = _mm_sub_pd(E_tmp_center, temp_1);

				_mm_storeu_pd(&E_tmp[i], E_tmp_center); 

				// E_tmp[i] += -dt*(kk*E_tmp[i]*(E_tmp[i]-a)*(E_tmp[i]-1)+E_tmp[i]*R_tmp[i]);

				temp_1 = _mm_add_pd(E_tmp_center, minus_b_minus_one_sse);		// E_tmp[i]-b-1
				temp_2 = _mm_mul_pd(kk_sse, E_tmp_center);				// kk*E_tmp[i]	
				temp_1 = _mm_mul_pd(temp_1, temp_2);					// kk*E_tmp[i]*(E_tmp[i]-b-1)
				temp_2 = _mm_sub_pd(zero_sse, R_tmp_center);				// -R_tmp[i]	
				temp_1 = _mm_sub_pd(temp_2, temp_1);					// -R_tmp[i]-kk*(E_tmp[i]-b-1)

				temp_2 = _mm_add_pd(E_tmp_center, M2_sse);				// E_tmp[i]+M2
				temp_3 = _mm_mul_pd(M1_sse, R_tmp_center);				// M1*R_tmp[i]
				temp_2 = _mm_div_pd(temp_3, temp_2);					// M1*R_tmp[i]/(E_tmp[i]+M2)
				temp_2 = _mm_add_pd(temp_2, epsilon_sse);				// eplison+M1*R_tmp[i]/(E_tmp[i]+M2)

				temp_1 = _mm_mul_pd(temp_1, temp_2);					// (eplison+M1*R_tmp[i]/(E_tmp[i]+M2))*(-R_tmp[i]-kk*(E_tmp[i]-b-1))
				temp_1 = _mm_mul_pd(temp_1, dt_sse);					// dt*(epsilon+M1*R_tmp[i]/(E_tmp[i]+M2))*(-R_tmp[i]-kk*(E_tmp[i]-b-1))

				R_tmp_center = _mm_add_pd(R_tmp_center, temp_1);

				_mm_storeu_pd(&R_tmp[i], R_tmp_center);

				// R_tmp[i] += dt*(epsilon+M1* R_tmp[i]/( E_tmp[i]+M2))*(-R_tmp[i]-kk*E_tmp[i]*(E_tmp[i]-b-1));
			}
		}
#endif

#endif

#ifdef AVX
		// #define FUSED 1
#ifdef FUSED
		// Solve for the excitation, a PDE
		for(j = innerBlockRowStartIndex; j <= innerBlockRowEndIndex; j+=(n+2)) {
			E_tmp = E + j;
			E_prev_tmp = E_prev + j;
			R_tmp = R + j;
			for(int i = 0; i < n; i++) {
				E_tmp[i] = E_prev_tmp[i]+alpha*(E_prev_tmp[i+1]+E_prev_tmp[i-1]-4*E_prev_tmp[i]+E_prev_tmp[i+(n+2)]+E_prev_tmp[i-(n+2)]);
				E_tmp[i] += -dt*(kk*E_tmp[i]*(E_tmp[i]-a)*(E_tmp[i]-1)+E_tmp[i]*R_tmp[i]);
				R_tmp[i] += dt*(epsilon+M1* R_tmp[i]/( E_tmp[i]+M2))*(-R_tmp[i]-kk*E_tmp[i]*(E_tmp[i]-b-1));
			}
		}
#else
		// Solve for the excitation, a PDE
		for(j = innerBlockRowStartIndex; j <= innerBlockRowEndIndex; j+=(n+2)) {
			E_tmp = E + j;
			E_prev_tmp = E_prev + j;
			for(int i = 0; i < n; i+=4) {
				// Storing the values of the left, right, top down E_prev entries in the sse registers 
				E_prev_center = _mm256_loadu_pd(E_prev_tmp + i);
				E_prev_left = _mm256_loadu_pd(E_prev_tmp + i - 1);
				E_prev_right = _mm256_loadu_pd(E_prev_tmp + i + 1);
				E_prev_above = _mm256_loadu_pd(E_prev_tmp + i - (n+2));
				E_prev_below = _mm256_loadu_pd(E_prev_tmp + i + (n+2));

				// Airthematic operations done to replicated the PDE done in the case without vectorisations
				temp_1 = _mm256_mul_pd(four_sse, E_prev_center);		// 4*E_prev_tmp[i]
				temp_2 = _mm256_add_pd(E_prev_right, E_prev_left);		// E_prev_tmp[i+1]+E_prev_tmp[i-1]
				temp_3 = _mm256_add_pd(E_prev_above, E_prev_below);	// E_prev_tmp[i-(n+2)]+E_prev_tmp[i+(n+2)]
				temp_2 = _mm256_add_pd(temp_2, temp_3);				
				temp_1 = _mm256_sub_pd(temp_2, temp_1);
				temp_1 = _mm256_mul_pd(temp_1, alpha_sse);
				temp_1 = _mm256_add_pd(E_prev_center, temp_1);

				// Storing the value back into the right pointer
				_mm256_storeu_pd(E_tmp + i, temp_1);

				//E_tmp[i] = E_prev_tmp[i]+alpha*(E_prev_tmp[i+1]+E_prev_tmp[i-1]-4*E_prev_tmp[i]+E_prev_tmp[i+(n+2)]+E_prev_tmp[i-(n+2)]);
			}
		}

		/* 
		 * Solve the ODE, advancing excitation and recovery variables
		 *     to the next timtestep
		 */

		for(j = innerBlockRowStartIndex; j <= innerBlockRowEndIndex; j+=(n+2)) {
			E_tmp = E + j;
			R_tmp = R + j;
			for(int i = 0; i < n; i+=4) {
				// Loading the pointer into 128 bit registers 
				E_tmp_center = _mm256_loadu_pd(E_tmp + i);
				R_tmp_center = _mm256_loadu_pd(R_tmp + i);	

				// Computation needed to replicate the functionality with vectorisation				
				temp_1 = _mm256_mul_pd(E_tmp_center, R_tmp_center);			// E_tmp[i]*R_tmp[i]
				temp_2 = _mm256_mul_pd(kk_sse, E_tmp_center);				// kk*E_tmp[i]
				temp_3 = _mm256_sub_pd(E_tmp_center, a_sse);				// E_tmp[i] - a
				temp_2 = _mm256_mul_pd(temp_2, temp_3);					// kk*E_tmp[i]*(E_tmp[i]-a)
				temp_3 = _mm256_sub_pd(E_tmp_center, one_sse);				// E_tmp[i] - 1
				temp_2 = _mm256_mul_pd(temp_2, temp_3);					// kk*E_tmp[i]*(E_tmp[i]-a)*(E_tmp[i]-1)
				temp_1 = _mm256_add_pd(temp_1, temp_2);					// kk*E_tmp[i]*(E_tmp[i]-a)*(E_tmp[i]-1) + E_tmp*R_tmp 
				temp_1 = _mm256_mul_pd(dt_sse, temp_1);					// dt*(kk*E_tmp[i]*(E_tmp[i]-a)*(E_tmp[i]-1)+E_tmp*R_tmp)

				E_tmp_center = _mm256_sub_pd(E_tmp_center, temp_1);

				_mm256_storeu_pd(&E_tmp[i], E_tmp_center); 

				// E_tmp[i] += -dt*(kk*E_tmp[i]*(E_tmp[i]-a)*(E_tmp[i]-1)+E_tmp[i]*R_tmp[i]);

				temp_1 = _mm256_add_pd(E_tmp_center, minus_b_minus_one_sse);		// E_tmp[i]-b-1
				temp_2 = _mm256_mul_pd(kk_sse, E_tmp_center);				// kk*E_tmp[i]	
				temp_1 = _mm256_mul_pd(temp_1, temp_2);					// kk*E_tmp[i]*(E_tmp[i]-b-1)
				temp_2 = _mm256_sub_pd(zero_sse, R_tmp_center);				// -R_tmp[i]	
				temp_1 = _mm256_sub_pd(temp_2, temp_1);					// -R_tmp[i]-kk*(E_tmp[i]-b-1)

				temp_2 = _mm256_add_pd(E_tmp_center, M2_sse);				// E_tmp[i]+M2
				temp_3 = _mm256_mul_pd(M1_sse, R_tmp_center);				// M1*R_tmp[i]
				temp_2 = _mm256_div_pd(temp_3, temp_2);					// M1*R_tmp[i]/(E_tmp[i]+M2)
				temp_2 = _mm256_add_pd(temp_2, epsilon_sse);				// eplison+M1*R_tmp[i]/(E_tmp[i]+M2)

				temp_1 = _mm256_mul_pd(temp_1, temp_2);					// (eplison+M1*R_tmp[i]/(E_tmp[i]+M2))*(-R_tmp[i]-kk*(E_tmp[i]-b-1))
				temp_1 = _mm256_mul_pd(temp_1, dt_sse);					// dt*(epsilon+M1*R_tmp[i]/(E_tmp[i]+M2))*(-R_tmp[i]-kk*(E_tmp[i]-b-1))

				R_tmp_center = _mm256_add_pd(R_tmp_center, temp_1);

				_mm256_storeu_pd(&R_tmp[i], R_tmp_center);

				// R_tmp[i] += dt*(epsilon+M1* R_tmp[i]/( E_tmp[i]+M2))*(-R_tmp[i]-kk*E_tmp[i]*(E_tmp[i]-b-1));
			}
		}
#endif
#endif

#else
// #define FUSED 1

#ifdef FUSED
    // Solve for the excitation, a PDE
    for(j = innerBlockRowStartIndex; j <= innerBlockRowEndIndex; j+=(n+2)) {
        E_tmp = E + j;
	E_prev_tmp = E_prev + j;
        R_tmp = R + j;
	for(i = 0; i < n; i++) {
	    E_tmp[i] = E_prev_tmp[i]+alpha*(E_prev_tmp[i+1]+E_prev_tmp[i-1]-4*E_prev_tmp[i]+E_prev_tmp[i+(n+2)]+E_prev_tmp[i-(n+2)]);
            E_tmp[i] += -dt*(kk*E_tmp[i]*(E_tmp[i]-a)*(E_tmp[i]-1)+E_tmp[i]*R_tmp[i]);
            R_tmp[i] += dt*(epsilon+M1* R_tmp[i]/( E_tmp[i]+M2))*(-R_tmp[i]-kk*E_tmp[i]*(E_tmp[i]-b-1));
        }
    }
#else
    // Solve for the excitation, a PDE
    for(j = innerBlockRowStartIndex; j <= innerBlockRowEndIndex; j+=(n+2)) {
        E_tmp = E + j;
            E_prev_tmp = E_prev + j;
            for(i = 0; i < n; i++) {
                E_tmp[i] = E_prev_tmp[i]+alpha*(E_prev_tmp[i+1]+E_prev_tmp[i-1]-4*E_prev_tmp[i]+E_prev_tmp[i+(n+2)]+E_prev_tmp[i-(n+2)]);
            }
    }

    /* 
     * Solve the ODE, advancing excitation and recovery variables
     *     to the next timtestep
     */

    for(j = innerBlockRowStartIndex; j <= innerBlockRowEndIndex; j+=(n+2)) {
        E_tmp = E + j;
        R_tmp = R + j;
        for(i = 0; i < n; i++) {
            E_tmp[i] += -dt*(kk*E_tmp[i]*(E_tmp[i]-a)*(E_tmp[i]-1)+E_tmp[i]*R_tmp[i]);
            R_tmp[i] += dt*(epsilon+M1* R_tmp[i]/( E_tmp[i]+M2))*(-R_tmp[i]-kk*E_tmp[i]*(E_tmp[i]-b-1));
        }
    }
#endif

#endif

		if (cb.stats_freq){
			if ( !(niter % cb.stats_freq)){
				stats(E,m,n,&mx,&sumSq);
#ifdef _MPI_
				if(!cb.noComm)
				{
					MPI_Reduce(&sumSq, &root_sumSq, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);
					MPI_Reduce(&Linf, &root_Linf, 1, MPI_DOUBLE, MPI_MAX, 0, MPI_COMM_WORLD);
				}
				else
					root_sumSq = sumSq;
#endif
				if(!(dy*cb.px+dx))
				{
					double l2norm = L2Norm(root_sumSq);
					repNorms(l2norm,mx,dt,cb.m,cb.n,niter, cb.stats_freq);
				}
			}
		}

		if (cb.plot_freq){
			if (!(niter % cb.plot_freq)){
				if(dy*cb.px + dx != 0)
				{
					for(int i = 1; i < m+1; i++)
					{
						MPI_Send(E + i*(n+2) + 1, n, MPI_DOUBLE, 0, i, MPI_COMM_WORLD);
					}

				}
				if(dy*cb.px + dx == 0)
				{
					for(int i = 1; i < m+1; i++)
					{
						for(j = 1; j < n + 1; j++)
						{
							Plot_E[i*(cb.n+2)+j] = E[i*(n+2) + j];
						}
					}
					
					for(int i = 1; i < cb.py*cb.px; i++)
					{
						int i_x = i%cb.px;
						int i_y = i/cb.px;
						int i_m = (cb.m/cb.py) + (i_y < cb.m%cb.py);
						int i_n = (cb.n/cb.px) + (i_x < cb.n%cb.px);
						int i_row_start = (cb.m/cb.py)*i_y + min(i_y, cb.m%cb.py) + 1;
						int i_column_start = (cb.n/cb.px)*i_x + min(i_y, cb.n%cb.px) + 1;

						for(int j = 0; j < i_m; j++)
						{
							MPI_Recv(Plot_E + (i_row_start+j)*(cb.n+2) + i_column_start, i_n, MPI_DOUBLE, i, j + 1, MPI_COMM_WORLD, MPI_STATUS_IGNORE);  
						}

					}

					for(int i = 0; i < cb.n + 2; i++)
						Plot_E[i] = Plot_E[2*(cb.n+2) + i];
					for(int i = cb.n + 1; i < (cb.m+2)*(cb.n+2); i += (cb.n+2))
						Plot_E[i] = Plot_E[i - 2];
					for(int i = 0; i < (cb.m+2)*(cb.n+2); i += (cb.n+2))
						Plot_E[i] = Plot_E[i + 2];
					for(int i = (cb.m+2)*(cb.n+2)-(cb.n+2); i < (cb.m+2)*(cb.n+2); i++)
						Plot_E[i] = Plot_E[i - 2*(cb.n+2)];
					plotter->updatePlot(Plot_E,  niter, cb.m, cb.n);
				}
			}
		}

		// Swap current and previous meshes
		double *tmp = E; E = E_prev; E_prev = tmp;

	} //end of 'niter' loop at the beginning

	// return the L2 and infinity norms via in-out parameters
	stats(E_prev,m,n,&Linf,&sumSq);
#ifdef _MPI_
	if(!cb.noComm)
	{
		MPI_Reduce(&sumSq, &root_sumSq, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);
		MPI_Reduce(&Linf, &root_Linf, 1, MPI_DOUBLE, MPI_MAX, 0, MPI_COMM_WORLD);
	}
	else
		root_sumSq = sumSq;
#endif
	if(!(dy*cb.px+dx))
	{
		Linf = root_Linf;
		L2 = L2Norm(root_sumSq);  
	}


	// Swap pointers so we can re-use the arrays
	*_E = E;
	*_E_prev = E_prev;
}
